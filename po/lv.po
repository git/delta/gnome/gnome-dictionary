# translation of lv.po to Latvian
# gnome-utils for Latvian
# Copyright (C) 2000, 2006, 2007, 2009 Free Software Foundation, Inc.
# Pēeris Krišjāis <peterisk@apollo.lv>, 2000.
# Artis Trops <hornet@navigators.lv>, 2000.
# sTYLEeX <styleex@inbox.lv>, 2006.
# Raivis Dejus <orvils@gmail.com>, 2006, 2007, 2009.
# Rūdolfs Mazurs <rudolfs.mazurs@gmail.com>, 2010, 2011.
# Rudolfs <rudolfs.mazurs@gmail.com>, 2011.
# Rūdofls Mazurs <rudolfs.mazurs@gmail.com>, 2011, 2012, 2013, 2014, 2016, 2017, 2019, 2021.
msgid ""
msgstr ""
"Project-Id-Version: lv\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/gnome-dictionary/issues\n"
"POT-Creation-Date: 2020-12-10 15:51+0000\n"
"PO-Revision-Date: 2021-11-01 22:22+0200\n"
"Last-Translator: Rūdolfs Mazurs <rudolfs.mazurs@gmail.com>\n"
"Language-Team: Latvian <lata-l10n@googlegroups.com>\n"
"Language: lv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n != 0 ? 1 :"
" 2);\n"
"X-Generator: Lokalize 21.08.1\n"

#: data/appdata/org.gnome.Dictionary.appdata.xml.in.in:6
msgid "GNOME Dictionary"
msgstr "GNOME vārdnīca"

# gdictsrc/gdict-app.c:454 gdictsrc/gdict-app.c:511
#: data/appdata/org.gnome.Dictionary.appdata.xml.in.in:7
#: data/org.gnome.Dictionary.desktop.in.in:4
msgid "Check word definitions and spellings in an online dictionary"
msgstr "Pārbauda vārdu skaidrojumus un rakstību interneta vārdnīcā"

#: data/appdata/org.gnome.Dictionary.appdata.xml.in.in:9
msgid ""
"GNOME Dictionary is a simple dictionary application that looks up "
"definitions of words online. Though it looks up English definitions by "
"default, you can easily switch to Spanish or add other online dictionaries "
"using the DICT protocol to suit your needs."
msgstr ""
"GNOME vārdnīca ir vienkārša vārdnīcas lietotne, kas tiešsaistē uzmeklē "
"vārdus. Pēc noklusējuma tā meklē angļu valodas definīcijas, bet jūs varat "
"viegli pārslēgties uz spāņu vai citām tiešsaistes vārdnīcām, izmantojot DICT "
"protokolu."

#: data/appdata/org.gnome.Dictionary.appdata.xml.in.in:34
msgid "The GNOME Project"
msgstr "GNOME projekts"

#: data/default.desktop.in:5
msgid "Default Dictionary Server"
msgstr "Noklusējuma vārdnīcas serveris"

#: data/org.gnome.Dictionary.desktop.in.in:3 src/gdict-about.c:62
#: src/gdict-app.c:369 src/gdict-window.c:495 src/gdict-window.c:1511
msgid "Dictionary"
msgstr "Vārdnīca"

#. Translators: Search terms to find this application. Do NOT translate or localize the semicolons! The list MUST also end with a semicolon!
#: data/org.gnome.Dictionary.desktop.in.in:6
msgid "word;synonym;definition;spelling;"
msgstr "vārds;sinonīms;definīcija;pareizrakstība;"

# gdictsrc/gdict-pref-dialog.c:178
#: data/org.gnome.dictionary.gschema.xml:6
msgid "The default database to use"
msgstr "Noklusējuma datubāze, ko lietot"

#: data/org.gnome.dictionary.gschema.xml:7
msgid ""
"The name of the default individual database or meta-database to use on a "
"dictionary source. An exclamation mark (“!”) means that all the databases "
"present in a dictionary source should be searched"
msgstr ""
"Noklusējuma individuālās datubāzes vai meta-datubāzes nosaukums, kuru "
"izmantot vārdnīcas avotā. Izsaukuma zīme (“!”) nozīmē, ka vajag pārmeklēt "
"visas datubāzes, kuras atrodas vārdnīcas avotā"

# gdictsrc/gdict-pref-dialog.c:178
#: data/org.gnome.dictionary.gschema.xml:11
msgid "The default search strategy to use"
msgstr "Noklusējuma meklēšanas stratēģija, ko lietot"

#: data/org.gnome.dictionary.gschema.xml:12
msgid ""
"The name of the default search strategy to use on a dictionary source, if "
"available. The default strategy is “exact”, that is match exact words."
msgstr ""
"Noklusējuma meklēšanas stratēģijas nosaukums, kuru izmantot vārdnīcas avotā, "
"ja tas ir pieejams. Noklusējuma stratēģija ir “exact”, kas nozīmē, lai vārdi "
"precīzi sakrīt."

#: data/org.gnome.dictionary.gschema.xml:16
msgid "The font to be used when printing"
msgstr "Fonts, ko izmantot drukāšanai"

#: data/org.gnome.dictionary.gschema.xml:17
msgid "The font to be used when printing a definition."
msgstr "Fonts, ko izmantot definīciju drukāšanai."

#: data/org.gnome.dictionary.gschema.xml:21
msgid "The name of the dictionary source used"
msgstr "Vārdnīcas avota nosaukums, kas tiek izmantots"

#: data/org.gnome.dictionary.gschema.xml:22
msgid ""
"The name of the dictionary source used to retrieve the definitions of words."
msgstr ""
"Vārdnīcas avota nosaukums, kas tiek izmantots, lai saņemtu vārdu definīcijas."

#. Translators: Do not translate the Name key
#: data/thai.desktop.in:4
msgid "Thai"
msgstr "Thai"

#: data/thai.desktop.in:5
msgid "Longdo Thai-English Dictionaries"
msgstr "Longdo taju-angļu vārdnīcas"

#: src/gdict-about.c:52
msgid "translator-credits"
msgstr ""
"Raivis Dejus <orvils@gmail.com>\n"
"Rūdolfs Mazurs <rudolfs.mazurs@gmail.com>"

# gdictsrc/gdict-app.c:454 gdictsrc/gdict-app.c:511
#: src/gdict-about.c:54
msgid "Look up words in dictionaries"
msgstr "Uzmeklēt vārdus vārdnīcās"

#: src/gdict-about.c:61
msgid "Dictionary (development build)"
msgstr "Vārdnīca (izstrādes būvējums)"

#: src/gdict-app-menus.ui:5
msgid "_New Window"
msgstr "Jau_ns logs"

#: src/gdict-app-menus.ui:12
msgid "_Save a Copy…"
msgstr "_Saglabāt kopiju…"

# gdictsrc/gdict-pref-dialog.c:161
#: src/gdict-app-menus.ui:18
msgid "P_review"
msgstr "P_riekšskatījums"

# gdictsrc/gdict-pref-dialog.c:161
#: src/gdict-app-menus.ui:23
msgid "_Print"
msgstr "_Drukāt"

#: src/gdict-app-menus.ui:30
msgid "_Find"
msgstr "_Meklēt"

#: src/gdict-app-menus.ui:37
msgid "_View"
msgstr "_Skats"

#: src/gdict-app-menus.ui:40
msgid "_Sidebar"
msgstr "_Sānjosla"

#: src/gdict-app-menus.ui:47
msgid "Similar _Words"
msgstr "Līdzīgie _vārdi"

#: src/gdict-app-menus.ui:52
msgid "Dictionary Sources"
msgstr "Vārdnīcas avoti"

#: src/gdict-app-menus.ui:57
msgid "Available St_rategies"
msgstr "Pieejamās st_ratēģijas"

#: src/gdict-app-menus.ui:64
msgid "_Go"
msgstr "_Iet"

#: src/gdict-app-menus.ui:67
msgid "_Previous Definition"
msgstr "Ie_priekšējā definīcija"

#: src/gdict-app-menus.ui:72
msgid "_Next Definition"
msgstr "_Nākamā definīcija"

#: src/gdict-app-menus.ui:79
msgid "_First Definition"
msgstr "Pirmā de_finīcija"

#: src/gdict-app-menus.ui:84
msgid "_Last Definition"
msgstr "Pē_dējā definīcija"

# gdictsrc/gdict-pref-dialog.c:129 gtt/options.c:518
# mini-utils/gcolorsel/menus.c:1109 mini-utils/gshutdown/gshutdown.c:314
#: src/gdict-app-menus.ui:93
msgid "Pr_eferences"
msgstr "I_estatījumi"

#: src/gdict-app-menus.ui:97
msgid "_Keyboard Shortcuts"
msgstr "_Tastatūras īsinājumtaustiņi"

# gcharmap/src/interface.c:208 gnome-find/gnome-find.c:89
# gstripchart/gstripchart.c:1784 gstripchart/gstripchart.c:2071
#. The help button is always visible
#: src/gdict-app-menus.ui:101 src/gdict-source-dialog.c:666
msgid "_Help"
msgstr "_Palīdzība"

#: src/gdict-app-menus.ui:106
msgid "_About Dictionary"
msgstr "P_ar vārdnīcu"

# gdictsrc/gdict-pref-dialog.c:147
#: src/gdict-app.c:45 src/gdict-app.c:74
msgid "Words to look up"
msgstr "Vārdi, ko uzmeklēt"

#: src/gdict-app.c:45 src/gdict-app.c:51
msgid "WORD"
msgstr "VĀRDS"

# gdictsrc/gdict-pref-dialog.c:147
#: src/gdict-app.c:51
msgid "Words to match"
msgstr "Meklējamie vārdi"

#: src/gdict-app.c:57
msgid "Dictionary source to use"
msgstr "Vārdnīcas avots, ko izmantot"

#: src/gdict-app.c:57 src/gdict-app.c:63 src/gdict-app.c:69
msgid "NAME"
msgstr "NOSAUKUMS"

# gdictsrc/gdict-pref-dialog.c:171
#: src/gdict-app.c:63
msgid "Database to use"
msgstr "Datubāze, ko izmantot"

# gdictsrc/gdict-pref-dialog.c:178
#: src/gdict-app.c:69
msgid "Strategy to use"
msgstr "Izmantojamā stratēģija"

#: src/gdict-app.c:74
msgid "WORDS"
msgstr "VĀRDI"

# gtt/toolbar.c:190
#: src/gdict-app.c:106
msgid "Dictionary Preferences"
msgstr "Vārdnīcas iestatījumi"

#: src/gdict-app.c:137 src/gdict-source-dialog.c:463
msgid "There was an error while displaying help"
msgstr "Gadījās kļūda, rādot palīdzību"

#: src/gdict-client-context.c:773
#, c-format
msgid "No connection to the dictionary server at “%s:%d”"
msgstr "Nav savienojuma ar vārdnīcas serveri vietā “%s:%d”"

#: src/gdict-client-context.c:1056
#, c-format
msgid "Lookup failed for hostname “%s”: no suitable resources found"
msgstr "Uzmeklēšana neizdevās serverim “%s” — netika atrasts piemērots resurss"

#: src/gdict-client-context.c:1087
#, c-format
msgid "Lookup failed for host “%s”: %s"
msgstr "Neizdevās uzmeklēšana serverim “%s” — %s"

#: src/gdict-client-context.c:1121
#, c-format
msgid "Lookup failed for host “%s”: host not found"
msgstr "Neizdevās uzmeklēšana serverim “%s” — serveris netika atrasts"

#: src/gdict-client-context.c:1173
#, c-format
msgid ""
"Unable to connect to the dictionary server at “%s:%d”. The server replied "
"with code %d (server down)"
msgstr ""
"Nevar savienoties ar vārdnīcas serveri vietā “%s:%d”. Serveris atgrieza kodu "
"%d (serveris nedarbojas)"

#: src/gdict-client-context.c:1192
#, c-format
msgid ""
"Unable to parse the dictionary server reply\n"
": “%s”"
msgstr ""
"Neizdodas apstrādāt vārdnīcas servera atbildi\n"
": “%s”"

#: src/gdict-client-context.c:1221
#, c-format
msgid "No definitions found for “%s”"
msgstr "Netika atrastas “%s” definīcijas"

#: src/gdict-client-context.c:1236
#, c-format
msgid "Invalid database “%s”"
msgstr "Nederīga datubāze “%s”"

#: src/gdict-client-context.c:1251
#, c-format
msgid "Invalid strategy “%s”"
msgstr "Nederīga stratēģija “%s”"

#: src/gdict-client-context.c:1266
#, c-format
msgid "Bad command “%s”"
msgstr "Slikta komanda “%s”"

#: src/gdict-client-context.c:1281
#, c-format
msgid "Bad parameters for command “%s”"
msgstr "Nepareizi parametri komandai “%s”"

#: src/gdict-client-context.c:1296
#, c-format
msgid "No databases found on dictionary server at “%s”"
msgstr "Netika atrastas datubāzes vārdnīcas serverī “%s”"

#: src/gdict-client-context.c:1311
#, c-format
msgid "No strategies found on dictionary server at “%s”"
msgstr "Netika atrastas stratēģijas vārdnīcas serverī “%s”"

#: src/gdict-client-context.c:1743
#, c-format
msgid "Connection failed to the dictionary server at %s:%d"
msgstr "Neizdevās savienojums ar vārdnīcas serveri %s:%d"

#: src/gdict-client-context.c:1782
#, c-format
msgid ""
"Error while reading reply from server:\n"
"%s"
msgstr ""
"Kļūda, lasot atbildi no servera:\n"
"%s"

#: src/gdict-client-context.c:1855
#, c-format
msgid "Connection timeout for the dictionary server at “%s:%d”"
msgstr "Savienojuma noildze ar vārdnīcas serveri “%s:%d”"

#: src/gdict-client-context.c:1889
#, c-format
msgid "No hostname defined for the dictionary server"
msgstr "Nav definēts datora nosaukums vārdnīcas serverim"

# mini-utils/idetool/idetool.c:41
#: src/gdict-client-context.c:1925 src/gdict-client-context.c:1940
#, c-format
msgid "Unable to create socket"
msgstr "Nevarēja izveidot ligzdu"

#: src/gdict-client-context.c:1966
#, c-format
msgid "Unable to set the channel as non-blocking: %s"
msgstr "Nevarēja iestatīt kanālu kā nebloķējošo — %s"

#: src/gdict-client-context.c:1981
#, c-format
msgid "Unable to connect to the dictionary server at “%s:%d”"
msgstr "Nevarēja savienoties ar vārdnīcas serveri vietā “%s:%d”"

#: src/gdict-database-chooser.c:372
msgid "Reload the list of available databases"
msgstr "Pārlādēt pieejamo datubāžu sarakstu"

#: src/gdict-database-chooser.c:384
msgid "Clear the list of available databases"
msgstr "Attīrīt pieejamo datubāžu sarakstu"

#: src/gdict-database-chooser.c:836 src/gdict-speller.c:766
#: src/gdict-strategy-chooser.c:773
msgid "Error while matching"
msgstr "Kļūda, meklējot atbilstību"

#: src/gdict-defbox.c:2388
msgid "Error while looking up definition"
msgstr "Kļūda, uzmeklējot definīciju"

#: src/gdict-defbox.c:2430 src/gdict-speller.c:724
msgid "Another search is in progress"
msgstr "Pašlaik darbojas cita meklēšana"

#: src/gdict-defbox.c:2431 src/gdict-speller.c:725
msgid "Please wait until the current search ends."
msgstr "Lūdzu, uzgaidiet, kamēr tekošā meklēšana beigsies."

#: src/gdict-defbox.c:2470
msgid "Error while retrieving the definition"
msgstr "Kļūda, saņemot definīciju"

#: src/gdict-help-overlay.ui:13
msgctxt "shortcut window"
msgid "General"
msgstr "Vispārīgi"

#: src/gdict-help-overlay.ui:18
msgctxt "shortcut window"
msgid "Show help"
msgstr "Rādīt palīdzību"

#: src/gdict-help-overlay.ui:24
msgctxt "shortcut window"
msgid "Show keyboard shortcuts"
msgstr "Rādīt tastatūras īsinājumtaustiņus"

#: src/gdict-help-overlay.ui:32
msgctxt "shortcut window"
msgid "Open a new window"
msgstr "Atvērt jaunu logu"

#: src/gdict-help-overlay.ui:39
msgctxt "shortcut window"
msgid "Close current window"
msgstr "Aizvērt pašreizējo logu"

#: src/gdict-help-overlay.ui:45
#| msgid "_Quit"
msgctxt "shortcut window"
msgid "Quit"
msgstr "Iziet"

# gdictsrc/gdict-pref-dialog.c:129 gtt/options.c:518
# mini-utils/gcolorsel/menus.c:1109 mini-utils/gshutdown/gshutdown.c:314
#: src/gdict-help-overlay.ui:53
#| msgid "Pr_eferences"
msgctxt "shortcut window"
msgid "Preferences"
msgstr "Iestatījumi"

#: src/gdict-help-overlay.ui:60
#| msgid "_Sidebar"
msgctxt "shortcut window"
msgid "View sidebar"
msgstr "Skatīt sānjoslu"

# gtt/toolbar.c:190
#: src/gdict-help-overlay.ui:68
#| msgid "Dictionary Preferences"
msgctxt "shortcut window"
msgid "Dictionary definitions"
msgstr "Vārdnīcas definīcijas"

#: src/gdict-help-overlay.ui:73
#| msgid "Similar words"
msgctxt "shortcut window"
msgid "Similar words"
msgstr "Līdzīgie vārdi"

#: src/gdict-help-overlay.ui:80
#| msgid "Dictionary sources"
msgctxt "shortcut window"
msgid "Dictionary sources"
msgstr "Vārdnīcu avoti"

#: src/gdict-help-overlay.ui:87
#| msgid "Available strategies"
msgctxt "shortcut window"
msgid "Available strategies"
msgstr "Pieejamās stratēģijas"

#: src/gdict-help-overlay.ui:95
msgctxt "shortcut window"
msgid "Navigate within a definition"
msgstr "Pārvietoties definīcijā"

#: src/gdict-help-overlay.ui:100
#| msgid "_Previous Definition"
msgctxt "shortcut window"
msgid "Previous definition"
msgstr "Iepriekšējā definīcija"

#: src/gdict-help-overlay.ui:107
#| msgid "_Next Definition"
msgctxt "shortcut window"
msgid "Next definition"
msgstr "Nākamā definīcija"

#: src/gdict-help-overlay.ui:114
#| msgid "_First Definition"
msgctxt "shortcut window"
msgid "First definition"
msgstr "Pirmā definīcija"

#: src/gdict-help-overlay.ui:121
#| msgid "_Last Definition"
msgctxt "shortcut window"
msgid "Last definition"
msgstr "Pēdējā definīcija"

#: src/gdict-help-overlay.ui:129
msgctxt "shortcut window"
msgid "Others"
msgstr "Citi"

#: src/gdict-help-overlay.ui:134
#| msgid "Error while looking up definition"
msgctxt "shortcut window"
msgid "Search within a definition"
msgstr "Meklēt definīcijā"

#: src/gdict-help-overlay.ui:141
msgctxt "shortcut window"
msgid "Find in text"
msgstr "Meklēt tekstā"

#: src/gdict-help-overlay.ui:148
#| msgid "Save a Copy"
msgctxt "shortcut window"
msgid "Save as copy"
msgstr "Saglabāt kā kopiju"

# gdictsrc/gdict-pref-dialog.c:161
#: src/gdict-help-overlay.ui:155
#| msgid "P_review"
msgctxt "shortcut window"
msgid "Print preview"
msgstr "Drukāšanas priekšskatījums"

# gdictsrc/gdict-pref-dialog.c:161
#: src/gdict-help-overlay.ui:162
#| msgid "Print"
msgctxt "shortcut window"
msgid "Print"
msgstr "Drukāt"

#: src/gdict-pref-dialog.c:260
msgid "View Dictionary Source"
msgstr "Skatīt vārdnīcas avotu"

#: src/gdict-pref-dialog.c:326
msgid "Add Dictionary Source"
msgstr "Pievienot vārdnīcas avotu"

# gnome-find/gnome-find.c:102 gsearchtool/gsearchtool.c:545
# logview/actions.c:127
#: src/gdict-pref-dialog.c:370
#, c-format
msgid "Remove “%s”?"
msgstr "Izņemt “%s”?"

#: src/gdict-pref-dialog.c:372
msgid "This will permanently remove the dictionary source from the list."
msgstr "Vārdnīcas avots tiks izņemts no saraksta pavisam."

#: src/gdict-pref-dialog.c:375 src/gdict-source-dialog.c:575
#: src/gdict-window.c:819
msgid "_Cancel"
msgstr "At_celt"

# gnome-find/gnome-find.c:102 gsearchtool/gsearchtool.c:545
# logview/actions.c:127
#: src/gdict-pref-dialog.c:376
msgid "_Remove"
msgstr "_Izņemt"

# mini-utils/gless/gless.c:617
#: src/gdict-pref-dialog.c:394
#, c-format
msgid "Unable to remove source “%s”"
msgstr "Nevar izņemt avotu “%s”"

#: src/gdict-pref-dialog.c:441
msgid "Edit Dictionary Source"
msgstr "Rediģēt vārdnīcas avotu"

#: src/gdict-pref-dialog.ui:34
msgid "_Select a dictionary source for looking up words:"
msgstr "Izvēlietie_s vārdnīcas avotu vārdu uzmeklēšanai:"

#: src/gdict-pref-dialog.ui:96
msgid "Add a new dictionary source"
msgstr "Pievienot jaunu vārdnīcas avotu"

#: src/gdict-pref-dialog.ui:116
msgid "Remove the currently selected dictionary source"
msgstr "Izņemt izvēlēto vārdnīcas avotu"

# logview/actions.c:120
#: src/gdict-pref-dialog.ui:136
msgid "Edit"
msgstr "Rediģēt"

#: src/gdict-pref-dialog.ui:137
msgid "Edit the currently selected dictionary source"
msgstr "Rediģēt izvēlēto vārdnīcas avotu"

# gtt/toolbar.c:152
#: src/gdict-pref-dialog.ui:157 src/gdict-source-dialog.ui:171
msgid "Source"
msgstr "Avots"

# gdictsrc/gdict-pref-dialog.c:161
#: src/gdict-pref-dialog.ui:187
msgid "_Print font:"
msgstr "_Drukāšanas fonts:"

#: src/gdict-pref-dialog.ui:216
msgid "Set the font used for printing the definitions"
msgstr "Iestatīt definīciju drukai lietojamo fontu"

# gdictsrc/gdict-pref-dialog.c:161
#: src/gdict-pref-dialog.ui:241
msgid "Print"
msgstr "Drukāt"

# mini-utils/gless/gless.c:617
#: src/gdict-print.c:235 src/gdict-print.c:299
#, c-format
msgid "Unable to display the preview: %s"
msgstr "Nevar parādīt priekšskatījumu — %s"

#: src/gdict-source-chooser.c:278
msgid "Reload the list of available sources"
msgstr "Pārlādēt pieejamo avotu sarakstu"

# mini-utils/idetool/idetool.c:41
#: src/gdict-source-dialog.c:314 src/gdict-source-dialog.c:408
msgid "Unable to create a source file"
msgstr "Nevar izveidot avota datni"

# logview/logrtns.c:129
#: src/gdict-source-dialog.c:336 src/gdict-source-dialog.c:428
msgid "Unable to save source file"
msgstr "Nevar saglabāt avota datni"

#. we just allow closing the dialog
#: src/gdict-source-dialog.c:571 src/gdict-source-dialog.c:585
msgid "_Close"
msgstr "_Aizvērt"

#: src/gdict-source-dialog.c:576
msgid "_Add"
msgstr "_Pievienot"

#: src/gdict-source-dialog.c:584
msgid "C_ancel"
msgstr "At_celt"

# logview/zoom.c:183
#: src/gdict-source-dialog.ui:58
msgid "_Description"
msgstr "_Apraksts"

# gdictsrc/gdict-pref-dialog.c:161
#: src/gdict-source-dialog.ui:73
msgid "_Port"
msgstr "_Ports"

#: src/gdict-source-dialog.ui:87
msgid "_Hostname"
msgstr "Servera n_osaukums"

#: src/gdict-source-dialog.ui:101
msgid "_Transport"
msgstr "_Transports"

#: src/gdict-source-dialog.ui:115
msgid "2628"
msgstr "2628"

#: src/gdict-source-dialog.ui:127
msgid "dict.org"
msgstr "dict.org"

#: src/gdict-source-dialog.ui:139
msgid "Source Name"
msgstr "Avota nosaukums"

#: src/gdict-source-dialog.ui:198
msgid "Dictionaries"
msgstr "Vārdnīcas"

# gdictsrc/gdict-pref-dialog.c:178
#: src/gdict-source-dialog.ui:223
msgid "Strategies"
msgstr "Stratēģijas"

#: src/gdict-source.c:414
#, c-format
msgid "Invalid transport type “%d”"
msgstr "Nederīgs transporta veids “%d”"

#: src/gdict-source.c:442
#, c-format
msgid "No “%s” group found inside the dictionary source definition"
msgstr "Netika atrasta grupa “%s” vārdnīcas avota definīcijā"

#: src/gdict-source.c:458 src/gdict-source.c:491 src/gdict-source.c:515
#: src/gdict-source.c:540
#, c-format
msgid "Unable to get the “%s” key inside the dictionary source definition: %s"
msgstr "Nevarēja iegūt “%s” atslēgu vārdnīcas avota definīcijā — %s"

#: src/gdict-source.c:565
#, c-format
msgid ""
"Unable to get the “%s” key inside the dictionary source definition file: %s"
msgstr "Nevarēja iegūt “%s” atslēgu vārdnīcas avota definīcijas datnē — %s"

#: src/gdict-source.c:759
#, c-format
msgid "Dictionary source does not have name"
msgstr "Vārdnīcas avotam nav nosaukuma"

#: src/gdict-source.c:768
#, c-format
msgid "Dictionary source “%s” has invalid transport “%s”"
msgstr "Vārdnīcas avotam “%s” ir nepareizs transports “%s”"

# gdictsrc/gdict-app.c:454 gdictsrc/gdict-app.c:511
#: src/gdict-speller.c:340
msgid "Clear the list of similar words"
msgstr "Attīrīt līdzīgo vārdu sarakstu"

#: src/gdict-strategy-chooser.c:348
msgid "Reload the list of available strategies"
msgstr "Pārlādēt pieejamo stratēģiju sarakstu"

#: src/gdict-strategy-chooser.c:359
msgid "Clear the list of available strategies"
msgstr "Attīrīt pieejamo stratēģiju sarakstu"

#: src/gdict-window.c:410
#, c-format
msgid "No dictionary source available with name “%s”"
msgstr "Nav pieejams vārdnīcas avots ar nosaukumu “%s”"

#: src/gdict-window.c:414
msgid "Unable to find dictionary source"
msgstr "Nevarēja atrast vārdnīcas avotu"

#: src/gdict-window.c:430
#, c-format
msgid "No context available for source “%s”"
msgstr "Nav pieejama konteksta avotam “%s”"

# mini-utils/idetool/idetool.c:41
#: src/gdict-window.c:434
msgid "Unable to create a context"
msgstr "Nevar izveidot kontekstu"

#: src/gdict-window.c:493
#, c-format
msgid "%s — Dictionary"
msgstr "%s — vārdnīca"

#: src/gdict-window.c:816
msgid "Save a Copy"
msgstr "Saglabāt kopiju"

#: src/gdict-window.c:820
msgid "_Save"
msgstr "_Saglabāt"

#: src/gdict-window.c:826
msgid "Untitled document"
msgstr "Nenosaukts dokuments"

#: src/gdict-window.c:847
#, c-format
msgid "Error while writing to “%s”"
msgstr "Kļūda, rakstot uz “%s”"

#. speller
#: src/gdict-window.c:1216
msgid "Double-click on the word to look up"
msgstr "Dubults klikšķis uz vārda, lai to uzmeklētu"

# gdictsrc/gdict-pref-dialog.c:178
#. strat-chooser
#: src/gdict-window.c:1222
msgid "Double-click on the matching strategy to use"
msgstr "Dubults klikšķis uz stratēģijas, lai to izmantotu"

# gdictsrc/gdict-pref-dialog.c:178
#. source-chooser
#: src/gdict-window.c:1227
msgid "Double-click on the source to use"
msgstr "Dubults klikšķis uz avota, lai to izmantotu"

#: src/gdict-window.c:1419
msgid "Similar words"
msgstr "Līdzīgie vārdi"

#: src/gdict-window.c:1450
msgid "Available strategies"
msgstr "Pieejamās stratēģijas"

#: src/gdict-window.c:1466
msgid "Dictionary sources"
msgstr "Vārdnīcu avoti"

#: src/gdict-utils.c:93
msgid "GDict debugging flags to set"
msgstr "Iestatāmie GDict atkļūdošanas karogi"

#: src/gdict-utils.c:93 src/gdict-utils.c:95
msgid "FLAGS"
msgstr "KAROGI"

#: src/gdict-utils.c:95
msgid "GDict debugging flags to unset"
msgstr "Atstatāmie GDict atkļūdošanas karogi"

#: src/gdict-utils.c:150
msgid "GDict Options"
msgstr "GDict opcijas"

# logview/actions.c:61 logview/monitor.c:115
#: src/gdict-utils.c:151
msgid "Show GDict Options"
msgstr "Rādīt GDict opcijas"

#~ msgid "Default"
#~ msgstr "Default"

#~ msgid "accessories-dictionary"
#~ msgstr "accessories-dictionary"

#~ msgid "spanish"
#~ msgstr "spanish"

#~ msgid "Spanish Dictionaries"
#~ msgstr "Spāņu vārdnīcas"

# gcharmap/src/interface.c:208 gnome-find/gnome-find.c:89
# gstripchart/gstripchart.c:1784 gstripchart/gstripchart.c:2071
#~ msgid "Help"
#~ msgstr "Palīdzība"

# gdictsrc/gdict-applet.c:130 mini-utils/gcolorsel/menus.c:1098
#~ msgid "About"
#~ msgstr "Par"

#~ msgid "_Save a Copy..."
#~ msgstr "_Saglabāt kopiju..."

#~ msgid "Available _Dictionaries"
#~ msgstr "Pieejamās vār_dnīcas"
